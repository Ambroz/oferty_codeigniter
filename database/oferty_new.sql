-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Czas generowania: 12 Wrz 2019, 22:04
-- Wersja serwera: 5.7.26
-- Wersja PHP: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `oferty`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id_category` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(150) NOT NULL,
  PRIMARY KEY (`id_category`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `category`
--

INSERT INTO `category` (`id_category`, `category_name`) VALUES
(1, 'Frontend (HTML, JS, JQuery, CSS)'),
(2, 'Backend (PHP Ruby on Rails, Node JS)'),
(3, 'Budownictwo');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `id_comment` int(11) NOT NULL AUTO_INCREMENT,
  `commenter_user_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `sallary` int(11) NOT NULL,
  `realization_time` int(11) NOT NULL,
  PRIMARY KEY (`id_comment`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `comments`
--

INSERT INTO `comments` (`id_comment`, `commenter_user_id`, `comment`, `sallary`, `realization_time`) VALUES
(2, 5, 'testowy komentarz', 1234, 7);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `country`
--

DROP TABLE IF EXISTS `country`;
CREATE TABLE IF NOT EXISTS `country` (
  `id_country` int(11) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(25) NOT NULL,
  `short_code` varchar(10) NOT NULL,
  PRIMARY KEY (`id_country`)
) ENGINE=InnoDB AUTO_INCREMENT=242 DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `country`
--

INSERT INTO `country` (`id_country`, `country_name`, `short_code`) VALUES
(1, 'Afghanistan', 'AF'),
(2, 'Albania', 'AL'),
(3, 'Algeria', 'DZ'),
(4, 'American Samoa', 'AS'),
(5, 'Andorra', 'AD'),
(6, 'Angola', 'AO'),
(7, 'Anguilla', 'AI'),
(8, 'Antarctica', 'AQ'),
(9, 'Antigua And Barbuda', 'AG'),
(10, 'Argentina', 'AR'),
(11, 'Armenia', 'AM'),
(12, 'Aruba', 'AW'),
(13, 'Australia', 'AU'),
(14, 'Austria', 'AT'),
(15, 'Azerbaijan', 'AZ'),
(16, 'Bahamas', 'BS'),
(17, 'Bahrain', 'BH'),
(18, 'Bangladesh', 'BD'),
(19, 'Barbados', 'BB'),
(20, 'Belarus', 'BY'),
(21, 'Belgium', 'BE'),
(22, 'Belize', 'BZ'),
(23, 'Benin', 'BJ'),
(24, 'Bermuda', 'BM'),
(25, 'Bhutan', 'BT'),
(26, 'Bolivia', 'BO'),
(27, 'Bosnia And Herzegovina', 'BA'),
(28, 'Botswana', 'BW'),
(29, 'Bouvet Island', 'BV'),
(30, 'Brazil', 'BR'),
(31, 'British Indian Ocean Terr', 'IO'),
(32, 'Brunei Darussalam', 'BN'),
(33, 'Bulgaria', 'BG'),
(34, 'Burkina Faso', 'BF'),
(35, 'Burundi', 'BI'),
(36, 'Cambodia', 'KH'),
(37, 'Cameroon', 'CM'),
(38, 'Canada', 'CA'),
(39, 'Cape Verde', 'CV'),
(40, 'Cayman Islands', 'KY'),
(41, 'Central African Republic', 'CF'),
(42, 'Chad', 'TD'),
(43, 'Chile', 'CL'),
(44, 'China', 'CN'),
(45, 'Christmas Island', 'CX'),
(46, 'Cocos (keeling) Islands', 'CC'),
(47, 'Colombia', 'CO'),
(48, 'Comoros', 'KM'),
(49, 'Congo', 'CG'),
(50, 'Congo, The Democratic Rep', 'CD'),
(51, 'Cook Islands', 'CK'),
(52, 'Costa Rica', 'CR'),
(53, 'Cote D\'ivoire', 'CI'),
(54, 'Croatia', 'HR'),
(55, 'Cuba', 'CU'),
(56, 'Cyprus', 'CY'),
(57, 'Czech Republic', 'CZ'),
(58, 'Denmark', 'DK'),
(59, 'Djibouti', 'DJ'),
(60, 'Dominica', 'DM'),
(61, 'Dominican Republic', 'DO'),
(62, 'East Timor', 'TP'),
(63, 'Ecuador', 'EC'),
(64, 'Egypt', 'EG'),
(65, 'El Salvador', 'SV'),
(66, 'Equatorial Guinea', 'GQ'),
(67, 'Eritrea', 'ER'),
(68, 'Estonia', 'EE'),
(69, 'Ethiopia', 'ET'),
(70, 'Falkland Islands (malvina', 'FK'),
(71, 'Faroe Islands', 'FO'),
(72, 'Fiji', 'FJ'),
(73, 'Finland', 'FI'),
(74, 'France', 'FR'),
(75, 'French Guiana', 'GF'),
(76, 'French Polynesia', 'PF'),
(77, 'French Southern Territori', 'TF'),
(78, 'Gabon', 'GA'),
(79, 'Gambia', 'GM'),
(80, 'Georgia', 'GE'),
(81, 'Germany', 'DE'),
(82, 'Ghana', 'GH'),
(83, 'Gibraltar', 'GI'),
(84, 'Greece', 'GR'),
(85, 'Greenland', 'GL'),
(86, 'Grenada', 'GD'),
(87, 'Guadeloupe', 'GP'),
(88, 'Guam', 'GU'),
(89, 'Guatemala', 'GT'),
(90, 'Guinea', 'GN'),
(91, 'Guinea-bissau', 'GW'),
(92, 'Guyana', 'GY'),
(93, 'Haiti', 'HT'),
(94, 'Heard Island And Mcdonald', 'HM'),
(95, 'Holy See (vatican City St', 'VA'),
(96, 'Honduras', 'HN'),
(97, 'Hong Kong', 'HK'),
(98, 'Hungary', 'HU'),
(99, 'Iceland', 'IS'),
(100, 'India', 'IN'),
(101, 'Indonesia', 'ID'),
(102, 'Iran, Islamic Republic Of', 'IR'),
(103, 'Iraq', 'IQ'),
(104, 'Ireland', 'IE'),
(105, 'Israel', 'IL'),
(106, 'Italy', 'IT'),
(107, 'Jamaica', 'JM'),
(108, 'Japan', 'JP'),
(109, 'Jordan', 'JO'),
(110, 'Kazakstan', 'KZ'),
(111, 'Kenya', 'KE'),
(112, 'Kiribati', 'KI'),
(113, 'Korea, Democratic People\'', 'KP'),
(114, 'Korea, Republic Of', 'KR'),
(115, 'Kosovo', 'KV'),
(116, 'Kuwait', 'KW'),
(117, 'Kyrgyzstan', 'KG'),
(118, 'Lao People\'s Democratic R', 'LA'),
(119, 'Latvia', 'LV'),
(120, 'Lebanon', 'LB'),
(121, 'Lesotho', 'LS'),
(122, 'Liberia', 'LR'),
(123, 'Libyan Arab Jamahiriya', 'LY'),
(124, 'Liechtenstein', 'LI'),
(125, 'Lithuania', 'LT'),
(126, 'Luxembourg', 'LU'),
(127, 'Macau', 'MO'),
(128, 'Macedonia, The Former Yug', 'MK'),
(129, 'Madagascar', 'MG'),
(130, 'Malawi', 'MW'),
(131, 'Malaysia', 'MY'),
(132, 'Maldives', 'MV'),
(133, 'Mali', 'ML'),
(134, 'Malta', 'MT'),
(135, 'Marshall Islands', 'MH'),
(136, 'Martinique', 'MQ'),
(137, 'Mauritania', 'MR'),
(138, 'Mauritius', 'MU'),
(139, 'Mayotte', 'YT'),
(140, 'Mexico', 'MX'),
(141, 'Micronesia, Federated Sta', 'FM'),
(142, 'Moldova, Republic Of', 'MD'),
(143, 'Monaco', 'MC'),
(144, 'Mongolia', 'MN'),
(145, 'Montserrat', 'MS'),
(146, 'Montenegro', 'ME'),
(147, 'Morocco', 'MA'),
(148, 'Mozambique', 'MZ'),
(149, 'Myanmar', 'MM'),
(150, 'Namibia', 'NA'),
(151, 'Nauru', 'NR'),
(152, 'Nepal', 'NP'),
(153, 'Netherlands', 'NL'),
(154, 'Netherlands Antilles', 'AN'),
(155, 'New Caledonia', 'NC'),
(156, 'New Zealand', 'NZ'),
(157, 'Nicaragua', 'NI'),
(158, 'Niger', 'NE'),
(159, 'Nigeria', 'NG'),
(160, 'Niue', 'NU'),
(161, 'Norfolk Island', 'NF'),
(162, 'Northern Mariana Islands', 'MP'),
(163, 'Norway', 'NO'),
(164, 'Oman', 'OM'),
(165, 'Pakistan', 'PK'),
(166, 'Palau', 'PW'),
(167, 'Palestinian Territory, Oc', 'PS'),
(168, 'Panama', 'PA'),
(169, 'Papua New Guinea', 'PG'),
(170, 'Paraguay', 'PY'),
(171, 'Peru', 'PE'),
(172, 'Philippines', 'PH'),
(173, 'Pitcairn', 'PN'),
(174, 'Poland', 'PL'),
(175, 'Portugal', 'PT'),
(176, 'Puerto Rico', 'PR'),
(177, 'Qatar', 'QA'),
(178, 'Reunion', 'RE'),
(179, 'Romania', 'RO'),
(180, 'Russian Federation', 'RU'),
(181, 'Rwanda', 'RW'),
(182, 'Saint Helena', 'SH'),
(183, 'Saint Kitts And Nevis', 'KN'),
(184, 'Saint Lucia', 'LC'),
(185, 'Saint Pierre And Miquelon', 'PM'),
(186, 'Saint Vincent And The Gre', 'VC'),
(187, 'Samoa', 'WS'),
(188, 'San Marino', 'SM'),
(189, 'Sao Tome And Principe', 'ST'),
(190, 'Saudi Arabia', 'SA'),
(191, 'Senegal', 'SN'),
(192, 'Serbia', 'RS'),
(193, 'Seychelles', 'SC'),
(194, 'Sierra Leone', 'SL'),
(195, 'Singapore', 'SG'),
(196, 'Slovakia', 'SK'),
(197, 'Slovenia', 'SI'),
(198, 'Solomon Islands', 'SB'),
(199, 'Somalia', 'SO'),
(200, 'South Africa', 'ZA'),
(201, 'South Georgia And The Sou', 'GS'),
(202, 'Spain', 'ES'),
(203, 'Sri Lanka', 'LK'),
(204, 'Sudan', 'SD'),
(205, 'Suriname', 'SR'),
(206, 'Svalbard And Jan Mayen', 'SJ'),
(207, 'Swaziland', 'SZ'),
(208, 'Sweden', 'SE'),
(209, 'Switzerland', 'CH'),
(210, 'Syrian Arab Republic', 'SY'),
(211, 'Taiwan, Province Of China', 'TW'),
(212, 'Tajikistan', 'TJ'),
(213, 'Tanzania, United Republic', 'TZ'),
(214, 'Thailand', 'TH'),
(215, 'Togo', 'TG'),
(216, 'Tokelau', 'TK'),
(217, 'Tonga', 'TO'),
(218, 'Trinidad And Tobago', 'TT'),
(219, 'Tunisia', 'TN'),
(220, 'Turkey', 'TR'),
(221, 'Turkmenistan', 'TM'),
(222, 'Turks And Caicos Islands', 'TC'),
(223, 'Tuvalu', 'TV'),
(224, 'Uganda', 'UG'),
(225, 'Ukraine', 'UA'),
(226, 'United Arab Emirates', 'AE'),
(227, 'United Kingdom', 'GB'),
(228, 'United States', 'US'),
(229, 'United States Minor Outly', 'UM'),
(230, 'Uruguay', 'UY'),
(231, 'Uzbekistan', 'UZ'),
(232, 'Vanuatu', 'VU'),
(233, 'Venezuela', 'VE'),
(234, 'Viet Nam', 'VN'),
(235, 'Virgin Islands, British', 'VG'),
(236, 'Virgin Islands, U.s.', 'VI'),
(237, 'Wallis And Futuna', 'WF'),
(238, 'Western Sahara', 'EH'),
(239, 'Yemen', 'YE'),
(240, 'Zambia', 'ZM'),
(241, 'Zimbabwe', 'ZW');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `is_active`
--

DROP TABLE IF EXISTS `is_active`;
CREATE TABLE IF NOT EXISTS `is_active` (
  `id_active` int(11) NOT NULL AUTO_INCREMENT,
  `active_name` varchar(5) NOT NULL,
  PRIMARY KEY (`id_active`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `is_active`
--

INSERT INTO `is_active` (`id_active`, `active_name`) VALUES
(1, 'Yes'),
(2, 'No');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `offers`
--

DROP TABLE IF EXISTS `offers`;
CREATE TABLE IF NOT EXISTS `offers` (
  `id_offers` int(11) NOT NULL AUTO_INCREMENT,
  `title_offers` text NOT NULL,
  `text_offers` text NOT NULL,
  `budget` int(11) NOT NULL,
  `expire_time` datetime NOT NULL,
  `work_mode` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  PRIMARY KEY (`id_offers`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `offers`
--

INSERT INTO `offers` (`id_offers`, `title_offers`, `text_offers`, `budget`, `expire_time`, `work_mode`, `category_id`, `status_id`) VALUES
(1, 'test', 'to jest oferta testowa', 1250, '2019-09-27 00:00:00', 1, 2, 1),
(2, 'test2', 'druga testowa oferta', 2500, '2019-09-30 00:00:00', 2, 1, 2),
(3, 'oferta zwykłego usera', 'testowa oferta napisana przez użytkownika', 1111, '2019-10-31 00:00:00', 1, 2, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `offer_comments`
--

DROP TABLE IF EXISTS `offer_comments`;
CREATE TABLE IF NOT EXISTS `offer_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `offer_id` int(11) NOT NULL,
  `comments_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `offer_comments`
--

INSERT INTO `offer_comments` (`id`, `offer_id`, `comments_id`) VALUES
(1, 3, 2);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `offer_user`
--

DROP TABLE IF EXISTS `offer_user`;
CREATE TABLE IF NOT EXISTS `offer_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `offer_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `offer_user`
--

INSERT INTO `offer_user` (`id`, `offer_id`, `user_id`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 5);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `plans`
--

DROP TABLE IF EXISTS `plans`;
CREATE TABLE IF NOT EXISTS `plans` (
  `plan_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `plan_description` text NOT NULL,
  `cost` float NOT NULL,
  PRIMARY KEY (`plan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `plans`
--

INSERT INTO `plans` (`plan_id`, `name`, `plan_description`, `cost`) VALUES
(1, 'Bronze', 'aa', 5),
(2, 'Silver', 'bb', 10.25),
(3, 'Gold', 'cc', 20.5),
(4, 'Platinum', 'dd', 41);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `status`
--

DROP TABLE IF EXISTS `status`;
CREATE TABLE IF NOT EXISTS `status` (
  `id_status` int(11) NOT NULL AUTO_INCREMENT,
  `status_name` varchar(30) NOT NULL,
  PRIMARY KEY (`id_status`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `status`
--

INSERT INTO `status` (`id_status`, `status_name`) VALUES
(1, 'Active'),
(2, 'On Hold'),
(3, 'Cancelled');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `type_of_comment`
--

DROP TABLE IF EXISTS `type_of_comment`;
CREATE TABLE IF NOT EXISTS `type_of_comment` (
  `id_type_of_comment` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(30) NOT NULL,
  PRIMARY KEY (`id_type_of_comment`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `type_of_comment`
--

INSERT INTO `type_of_comment` (`id_type_of_comment`, `type_name`) VALUES
(1, 'Positive'),
(2, 'Neutral'),
(3, 'Negative');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `login_email` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `address` text,
  `country_id` int(11) DEFAULT NULL,
  `telephon` int(11) DEFAULT NULL,
  `nip` int(11) DEFAULT NULL,
  `user_group_id` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `user`
--

INSERT INTO `user` (`id_user`, `login_email`, `password`, `first_name`, `last_name`, `address`, `country_id`, `telephon`, `nip`, `user_group_id`, `is_active`) VALUES
(1, 'n7komandore@gmail.com', '$2y$10$QFfzCdadrJebi7nYyoqgWuiH1M5lKPNBC8dH1tbdxofYs2LHJ75/.', 'Paweł', 'Ambroziak', '', 174, NULL, NULL, 1, 1),
(2, 'interiaset@gmail.com', '$2y$10$CFAskKyKrqEAyGtvui6cReAEBZIOXezApE7uaLV8hFqcplRKj9TKK', 'Kamil', 'Biszczak', '', 174, NULL, NULL, 1, 1),
(5, 'ambro8@wp.pl', '$2y$10$J1vZcnsQ7RJU9q3.B9dxD./uhVZaNWgEDyzIH2mO0k58zlzSJZFpu', 'Paw', 'A', '', 174, 0, 0, 2, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user_comments`
--

DROP TABLE IF EXISTS `user_comments`;
CREATE TABLE IF NOT EXISTS `user_comments` (
  `id_user_comments` int(11) NOT NULL AUTO_INCREMENT,
  `commenter_id` int(11) NOT NULL,
  `comment_text` int(11) NOT NULL,
  `type_of_comment_id` int(11) NOT NULL,
  PRIMARY KEY (`id_user_comments`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user_group`
--

DROP TABLE IF EXISTS `user_group`;
CREATE TABLE IF NOT EXISTS `user_group` (
  `id_user_group` int(11) NOT NULL AUTO_INCREMENT,
  `user_group_name` varchar(20) NOT NULL,
  PRIMARY KEY (`id_user_group`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `user_group`
--

INSERT INTO `user_group` (`id_user_group`, `user_group_name`) VALUES
(1, 'Admin'),
(2, 'User');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user_plan_time`
--

DROP TABLE IF EXISTS `user_plan_time`;
CREATE TABLE IF NOT EXISTS `user_plan_time` (
  `user_plan_time_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `plan_id` int(11) NOT NULL,
  `purchase_date` date NOT NULL,
  `expire_date` date NOT NULL,
  PRIMARY KEY (`user_plan_time_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user_user_comments`
--

DROP TABLE IF EXISTS `user_user_comments`;
CREATE TABLE IF NOT EXISTS `user_user_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `user_comments_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `work_mode`
--

DROP TABLE IF EXISTS `work_mode`;
CREATE TABLE IF NOT EXISTS `work_mode` (
  `id_work_mode` int(11) NOT NULL AUTO_INCREMENT,
  `work_mode_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id_work_mode`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `work_mode`
--

INSERT INTO `work_mode` (`id_work_mode`, `work_mode_name`) VALUES
(1, 'At Location'),
(2, 'Remote');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
