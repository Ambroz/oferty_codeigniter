<?php
class Admin_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
  }

  public function get_all_users(){
    $all_users =
    $this->db->SELECT('id_user, first_name, last_name, login_email, address, country.country_name, telephon, nip, user_group.user_group_name, is_active.active_name')
    ->FROM('user')
    ->JOIN('country', 'id_country = country_id')
    ->JOIN('user_group', 'id_user_group = user_group_id')
    ->JOIN('is_active', 'id_active = is_active')
    ->get()->result();

    return $all_users;
  }

  public function get_edit_plan($id){
    $get_edit_plan =
    $this->db->SELECT('*')
    ->FROM('plans')
    ->WHERE('plan_id', $id)
    ->get()->result();

    return $get_edit_plan;
  }

  public function get_offer_categories(){
    $get_offer_categories = $this->db->SELECT('category_name')
    ->FROM('category')
    ->get()->result();

    return $get_offer_categories;
  }

  public function get_num_offer_from_cat($category){
    $get_num_offer_from_cat = $this->db->SELECT('*')
    ->FROM('offers')
    ->JOIN('category', 'id_category = category_id')
    ->WHERE('category_name', $category)
    // ->get()->result();
    ->count_all_results();
    // $num_rows = $get_num_offer_from_cat->num_rows();

    return $get_num_offer_from_cat;
  }

  public function add_category_to_db($category){
    $add = array(
      'category_name' => $category
    );
    $add_category_to_db = $this->db->INSERT('category', $add);
    return $add_category_to_db;
  }

  public function get_report_amount(){
    $report_amount = $this->db->SELECT('*')
    ->FROM('reports')
    ->WHERE('seen_id', '2')
    ->count_all_results();

    return $report_amount;
  }

  public function get_all_reports(){
    $get_all_reports = $this->db->SELECT('user.login_email, user.first_name, user.last_name, country.short_code, who_reported_id, who_is_reported_id, report_id, seen_id, seen_name, amount_of_strikes')
    // $get_all_reports = $this->db->SELECT('*')
    // $get_all_reports = $this->db->SELECT('user.')
    ->JOIN('reports', 'report_id = id_report')
    // ->JOIN('user', 'user.id_user = user_report.who_reported_id AND user.id_user = reports.who_is_reported_id')
    // ->JOIN('user', 'id_user = who_is_reported_id OR id_user = who_reported_id')
    // ->JOIN('user', 'user.id_user = reports.who_is_reported_id')
    ->JOIN('user', 'id_user = who_is_reported_id')
    ->JOIN('country', 'id_country = country_id')
    ->JOIN('report_seen', 'id_seen = seen_id')
    ->FROM('user_report')
    ->get()->result();

    // var_dump($get_all_reports);
    return $get_all_reports;
  }

  public function get_spec_report_info($report_id){
    // $get_spec_report_info = $this->db->SELECT('*')
    $get_spec_report_info = $this->db->SELECT('id_user, login_email, first_name, last_name, report_id, report_text, short_code, id_seen, seen_name, admin_mod_text, who_reported_id')
    ->JOIN('reports', 'report_id = id_report')
    ->JOIN('user', 'id_user = who_reported_id')
    ->JOIN('country', 'id_country = country_id')
    ->JOIN('report_seen', 'id_seen = seen_id')
    ->FROM('user_report')
    ->WHERE('report_id', $report_id)
    ->get()->result();

    return $get_spec_report_info;
  }
  //
  // public function delete_rep_and_strike($report_id, $who_is_reported){
  //   $amount_strikes = $this->db->SELECT('amount_of_strikes')
  //   ->FROM('user')
  //   ->WHERE('id_user', $who_is_reported)
  //   ->get()->result();
  //
  //   //delete report
  //   $this->db->where('id', $report_id);
  //   $this->db->delete('reports');
  //
  //   $this->db->where('id', $report_id);
  //   $this->db->delete('user_report');
  //
  //   //update amount of strikes
  //   if($amount_strikes[0]->amount_of_strikes <= 0){
  //     $new_amount = 0;
  //   }else{
  //     $new_amount =  $amount_strikes[0]->amount_of_strikes - 1;
  //   }
  //   //update amount of strikes
  //   $data = array(
  //     'amount_of_strikes' => $new_amount
  //   );
  //
  //   $this->db->where('id_user', $who_is_reported);
  //   $this->db->update('user', $data);
  //   redirect('administrator/main/get_all_reports');
  // }
  //
  // public function delete_rep($report_id, $who_is_reported){
  //   // $this->db->where('id', $id);
  //   // $this->db->delete('mytable');
  //
  //   redirect('administrator/main/get_all_reports');
  // }

  public function delete_strike($report_id, $who_is_reported, $admin_mod_text){
    $amount_strikes = $this->db->SELECT('amount_of_strikes')
    ->FROM('user')
    ->WHERE('id_user', $who_is_reported)
    ->get()->result();

    if($amount_strikes[0]->amount_of_strikes <= 0){
      $new_amount = 0;
    }else{
      $new_amount =  $amount_strikes[0]->amount_of_strikes - 1;
    }
    //update amount of strikes
    $data_strike = array(
      'amount_of_strikes' => $new_amount,
      'seen_id' => 1
    );

    $this->db->where('id_user', $who_is_reported);
    $this->db->update('user', $data_strike);

    $data_text = array(
      'admin_mod_text' => $admin_mod_text
    );

    $this->db->where('id_report', $report_id);
    $this->db->update('reports', $data_text);

    redirect('administrator/main/get_all_reports');
  }

  public function add_rep_text($report_id, $who_is_reported, $admin_mod_text){
    $data = array(
      'admin_mod_text' => $admin_mod_text,
      'seen_id' => 1
    );


    $this->db->where('id_report', $report_id);
    $this->db->update('reports', $data);

    redirect('administrator/main/get_all_reports');
  }
}
