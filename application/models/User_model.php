<?php
// echo password_hash('test', PASSWORD_BCRYPT);
// $storred_pass = '$2y$10$qnGMYxNaLl2sVUKq/BRjMO5DLL3J53MDFEg.XI60m9mgAwJne6fuS';
//
// if(password_verify('test', $storred_pass)){
// 	echo 'Jest';
// }else{
// 	echo "Nie ma";
// }
// $user = $this->db->where('login_email', $email)->get('user')->row();

// if (isset($user) && password_verify($password, $user->password)){


class User_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
  }

  public function login($email, $password){

    $user = $this->db->where('login_email', $email)->get('user')->row();


    if (isset($user) && password_verify($password, $user->password)){

      if ($user->is_active == 1)
      {
        $login = new stdClass();
        $login->id_user = $user->id_user;
        $login->login_email = $user->login_email;
        $login->first_name = $user->first_name;
        $login->last_name = $user->last_name;
        $login->user_group_id = $user->user_group_id;
        return $login;
      }
      return 'inactive';
    }
    return false;
  }

  public function register($email, $password, $firstname, $lastname, $user_group_id, $is_active, $country){
    $user = $this->db->where('login_email', $email)->get('user')->row();
    // echo $user;
    if(isset($user)){

      return 'already_registred';
    }else{
      $pass = password_hash($password, PASSWORD_BCRYPT);
      $data = array(
        'login_email' => $email,
        'password' => $pass,
        'first_name' => $firstname,
        'last_name' => $lastname,
        'user_group_id' => $user_group_id,
        'is_active' => $is_active,
        'country_id' => $country
      );
      $this->db->insert('user', $data);
      $this->session->set_flashdata('message', "Konto zostalo zalozone pomyslnie");
      redirect('');
    }
  }

  public function get_country(){
    $data = array();
    $country = $this->db->select('id_country, country_name')
    ->from('country')
    ->get()->result();
    return $country;
  }

  public function get_user_details($id){
    $get_user =
    $this->db->SELECT('id_user, first_name, last_name, login_email, address, country.id_country, country.country_name, telephon, nip, user_group.user_group_name, is_active.active_name')
    ->FROM('user')
    ->JOIN('country', 'id_country = country_id')
    ->JOIN('user_group', 'id_user_group = user_group_id')
    ->JOIN('is_active', 'id_active = is_active')
    ->where('id_user', $id)
    ->get()->result();

    return $get_user;
  }

  public function get_plans(){
    $get_plan_details =
    $this->db->SELECT('*')
    ->FROM('plans')
    ->get()->result();

    return $get_plan_details;
  }

  public function get_offers(){
    $get_offer_info =
    $this->db->SELECT('id_offers, title_offers, text_offers, budget, expire_time, work_mode.work_mode_name, category.category_name, status_name, status_id')
    ->FROM('offers')
    ->JOIN('category', 'id_category = category_id ')
    ->JOIN('work_mode', 'id_work_mode = work_mode ')
    ->JOIN('status', 'id_status = status_id ')
    ->get()->result();

    return $get_offer_info;
  }

  public function create_offer($user_add_offer, $offer_title, $work_mode, $category, $budget, $expire_time, $job_description){

    $data = array(
      'title_offers' => $offer_title,
      'text_offers' => $job_description,
      'budget' => $budget,
      'expire_time' => $expire_time,
      'work_mode' => $work_mode,
      'category_id' => $category
    );

    $insert_offer =
    $this->db->INSERT('offers', $data);

    $last_insert_offer_id = $this->db->insert_id();

    $data2 = array(
      'offer_id' => $last_insert_offer_id,
      'user_id' => $user_add_offer
    );

    $user_offer =
    $this->db->INSERT('offer_user', $data2);

    $this->session->set_flashdata('message', "Oferta Została pomyślnie dodana.");
    redirect('account/create_offer');
  }

  public function get_offer_category(){
    $get_offer_category =
    $this->db->SELECT('*')
    ->FROM('category')
    ->get()->result();

    return $get_offer_category;
  }

  public function get_work_mode(){
    $get_work_mode =
    $this->db->SELECT('*')
    ->FROM('work_mode')
    ->get()->result();

    return $get_work_mode;
  }

  public function get_my_offers($id){
    $get_my_offers =
    $this->db->SELECT('id_offers, title_offers, text_offers, budget, expire_time, work_mode.work_mode_name, category.category_name, status_name')
    ->FROM('offer_user')
    ->JOIN('offers', 'id_offers = offer_id')
    ->JOIN('category', 'id_category = category_id ')
    ->JOIN('work_mode', 'id_work_mode = work_mode ')
    ->JOIN('status', 'id_status = status_id ')
    ->WHERE('user_id', $id)
    ->get()->result();

    return $get_my_offers;

  }

  public function user_update($data, $id, $edit_user){
    $this->db->WHERE('id_user', $edit_user)
    ->UPDATE('user', $data);

    if($id == 1){
      $this->session->set_flashdata('message', "Profil Zaktualizowano");
      redirect('administrator/main/show_user_list');
    }

    if($id == 2){
      $this->session->set_flashdata('message', "Profil Zaktualizowano");
      redirect('user/user_profile/edit_profile'.$loginData->id_user);
    }
  }

  public function get_info_of_selected_offer($offer_id){
    $get_offer = $this->db->SELECT('id_offers, title_offers, text_offers, budget, expire_time, work_mode.work_mode_name, category.category_name, status.status_name, status_id, user_id')
    ->FROM('offer_user')
    ->JOIN('offers', 'id_offers = offer_id')
    ->JOIN('category', 'id_category = category_id ')
    ->JOIN('work_mode', 'id_work_mode = work_mode ')
    ->JOIN('status', 'id_status = status_id ')
    ->WHERE('id_offers', $offer_id)
    ->get()->result();

    return $get_offer;
  }

  public function add_comm_to_db($comm_data, $sel_offer_id, $title_offer){

    $title = str_replace(' ', '_', $title_offer);

    $this->db->INSERT('comments', $comm_data);
    $comm_insert = $this->db->insert_id();

    $ins_data = array(
      'offer_id' => $sel_offer_id,
      'comments_id' => $comm_insert
    );

    $this->db->INSERT('offer_comments', $ins_data);
    $this->session->set_flashdata('message', "Comment Aded Sucessfully");
    redirect('main/show_offer/'.$title.'/'.$sel_offer_id);
  }

  public function sel_comments($offer_id){
    $selected_comm = $this->db->SELECT('id_user, first_name, last_name, country_name, comment, sallary, realization_time')
    // $selected_comm = $this->db->SELECT('*')
    ->FROM('offer_comments')
    ->JOIN('comments', 'id_comment = comments_id ')
    ->JOIN('user', 'id_user = commenter_user_id')
    ->JOIN('country', 'id_country = country_id')
    // ->JOIN('work_mode', 'id_work_mode = work_mode ')
    ->WHERE('offer_id', $offer_id)
    ->get()->result();

    return $selected_comm;
  }

  public function get_my_profile_info($id){
    $get_my_profile_info = $this->db->SELECT('id_user, first_name, last_name, address, telephon, nip, country.country_name')
    ->FROM('user')
    ->JOIN('country', 'country_id = id_country')
    ->WHERE('id_user', $id)
    ->get()->result();

    return $get_my_profile_info;
  }

  public function add_user_comm_to_db($id_commenting, $comm_desc, $comm_type, $id_commented, $full_name, $date){
    $ins_user_comm_data = array(
      'commenter_id' => $id_commenting,
      'comment_text' => $comm_desc,
      'type_of_comment_id' => $comm_type,
      'date_of_comment' => $date
    );

    $this->db->INSERT('user_comments', $ins_user_comm_data);

    $user_comm_insert = $this->db->insert_id();

    $ins_user_user_comm_data = array(
      'user_id' => $id_commented,
      'user_comments_id' => $user_comm_insert
    );

    $this->db->INSERT('user_user_comments', $ins_user_user_comm_data);
    $this->session->set_flashdata('message', "Comment Aded Sucessfully");
    redirect('user/user_profile/user_show_profile/'.$full_name.'/'.$id_commented);
  }

  public function get_user_comments($id){
    $get_user_comments = $this->db->SELECT('id_user, first_name, last_name, commenter_id, comment_text, type_of_comment_id, type_name, date_of_comment')
    ->FROM('user_user_comments')
    ->JOIN('user', 'id_user = user_id')
    ->JOIN('user_comments', 'id_user_comments = user_comments_id')
    ->JOIN('type_of_comment', 'id_type_of_comment = type_of_comment_id')
    ->WHERE('user_id', $id)
    ->get()->result();

    return $get_user_comments;
  }

  public function add_user_report($who_is_reported, $who_reported, $report, $date_of_report, $full_name, $seen){


    $report = array(
      'who_is_reported_id' => $who_is_reported,
      'report_text' => $report,
      'report_date' => $date_of_report,
      'seen_id' => $seen
    );

    $this->db->INSERT('reports', $report);

    $get_last_report = $this->db->insert_id();

    $user_report = array(
      'who_reported_id' => $who_reported,
      'report_id' => $get_last_report
    );

    $this->db->INSERT('user_report', $user_report);
    $this->session->set_flashdata('message', "Report Successfull");
    redirect('user/user_profile/user_show_profile/'.$full_name.'/'.$who_is_reported);
  }

  public function gen_token($email){
    $user = $this->db->where('login_email', $email)->get('user')->row();
    if(isset($user)){
      $de = date("Y-m-d").$email;
      $token = password_hash($de, PASSWORD_BCRYPT);
      $good_token = str_replace("$", "O", $token);

      $data = array(
        'token' => $good_token
      );

      $this->db->WHERE('login_email', $email)
      ->UPDATE('user', $data);

      $user = $this->db->where('login_email', $email)->get('user')->row();
      if(isset($user->token)){
        // $this->session->set_flashdata('message', 'Soon there sould arrive an e-mail with the reset link');

        return $user->token;
      }
    }else{
      $this->session->set_flashdata('message', "There is no user of that E-Mail");
      redirect("account/forgot_password");
    }

  }

  public function update_pass($password, $token){
    $user = $this->db->where('token', $token)->get('user')->row();
    if(isset($user)){
      $pass = password_hash($password, PASSWORD_BCRYPT);

      $data = array(
        'password' => $pass
      );

      $this->db->WHERE('token', $token)
      ->UPDATE('user', $data);

      $data2 = array(
          'token' => NULL
      );

      $this->db->WHERE('token', $token)
      ->UPDATE('user', $data2);
    }
  }

  public function count_coms($id){
    $get_user_pos_comments = $this->db->SELECT('id_user, first_name, last_name, commenter_id, comment_text, type_of_comment_id, type_name, date_of_comment')
    ->FROM('user_user_comments')
    ->JOIN('user', 'id_user = user_id')
    ->JOIN('user_comments', 'id_user_comments = user_comments_id')
    ->JOIN('type_of_comment', 'id_type_of_comment = type_of_comment_id')
    ->WHERE('user_id', $id)
    ->where('id_type_of_comment', '1')
    ->count_all_results();

    $get_user_neu_comments = $this->db->SELECT('id_user, first_name, last_name, commenter_id, comment_text, type_of_comment_id, type_name, date_of_comment')
    ->FROM('user_user_comments')
    ->JOIN('user', 'id_user = user_id')
    ->JOIN('user_comments', 'id_user_comments = user_comments_id')
    ->JOIN('type_of_comment', 'id_type_of_comment = type_of_comment_id')
    ->WHERE('user_id', $id)
    ->where('id_type_of_comment', '2')
    ->count_all_results();

    $get_user_neg_comments = $this->db->SELECT('id_user, first_name, last_name, commenter_id, comment_text, type_of_comment_id, type_name, date_of_comment')
    ->FROM('user_user_comments')
    ->JOIN('user', 'id_user = user_id')
    ->JOIN('user_comments', 'id_user_comments = user_comments_id')
    ->JOIN('type_of_comment', 'id_type_of_comment = type_of_comment_id')
    ->WHERE('user_id', $id)
    ->where('id_type_of_comment', '3')
    ->count_all_results();
    // ->get()->result();

    $comms = array(
      'positive' => $get_user_pos_comments,
      'neutral' => $get_user_neu_comments,
      'negative' => $get_user_neg_comments
    );

    return $comms;
  }

  // public function count_coms_neut($id){
  //
  // }
  //
  // public function count_coms_neg($id){
  //
  // }
}

?>
