<div class="uk-container uk-container-center">

  <a class="uk-button uk-button-default" href="<?=base_url()?>">Back</a>
  <ul>
    <?php

    // echo '<pre>';
    // print_r($sel_offer);
    // echo '</pre>';
    foreach ($sel_offer as $key => $offer_det) {
      if($offer_det->user_id == $loginData->id_user && $offer_det->status_id == 2){?>
        <a href="#" class="uk-button uk-button-default">Renew Offer</a>
        <?php
      }
      ?>

      <li><?=$offer_det->title_offers?></li>
      <li><?=$offer_det->text_offers?></li>
      <li><?=$offer_det->budget?></li>
      <li><?=$offer_det->expire_time?></li>
      <li><?=$offer_det->work_mode_name?></li>
      <li><?=$offer_det->category_name?></li>
      <li class="<?=str_replace(' ', '', strtolower($offer_det->status_name))?>"><?=$offer_det->status_name?></li>

      <?php
      $stat_info = $offer_det->status_id;
    }
    ?>
  </ul>
  <?php
  if($stat_info == 2 || $stat_info == 3){
    echo "<p>The Offer isn't active</p>";
  }else if($loginData == ""){
    echo "<p>You need to be Loged In to add comments</p>";
  }else{
    echo "<p>If you want to start in the contest, leave a comment.</p>";
    echo form_open(base_url().'main/add_comm_to_offer', 'class="uk-form " id="myform"');

    echo form_hidden('user_id', $loginData->id_user);
    echo form_hidden('sel_offer_id', $sel_offer[0]->id_offers);
    echo form_hidden('title_offer', $sel_offer[0]->title_offers);
    echo form_input(array('id' => 'first_last_name', 'name' => 'first_last_name','placeholder' => 'first_last_name', 'value' => $loginData->first_name." ".$loginData->last_name, 'readonly' => 'true'));
    echo '</br>';
    echo form_input(array('id' => 'sallary', 'name' => 'sallary', 'placeholder' => 'sallary', 'type' => 'number'));
    echo '</br>';
    echo form_input(array('id' => 'time_duration', 'name' => 'time_duration', 'type' => 'number', 'placeholder' => 'Job Duration'));
    echo '</br>';
    echo form_textarea(array('id' => 'comment_description', "class" => "editor", 'name' => 'comment_description', 'placeholder' => 'Comment'));
    echo '</br>';
    echo form_hidden('comment', '1');
    echo form_submit(array('id' => 'comment_submit', 'value' => 'Send', 'class' => 'uk-button'));
    echo '</p>';
    echo form_close();
  }
  ?>

  <?php
  // echo "<pre>";
  // print_r($sel_comm);
  // echo '</pre>';
  ?>
  <p>Comment Section</p>
  <ul>
    <?php
    foreach ($sel_comm as $key => $value) {
      if($value->id_user == $loginData->id_user){?>
        <li><?=$value->first_name." ".$value->last_name?></li>
        <?php
      }else{
        ?>
        <li><a href="<?=base_url()."user/user_profile/user_show_profile/".$value->first_name."_".$value->last_name."/".$value->id_user?>"><?=$value->first_name." ".$value->last_name?></a></li>
        <?php }?>
        <li><?=$value->country_name?></li>
        <li><?=$value->comment?></li>
        <li><?=$value->sallary?></li>
        <li><?=$value->realization_time?> Days</li>
        <?php
      }


    ?>
  </ul>
</div>
