<div class="forgot_pass_wrapper">
  <div class="uk-container uk-container-center">
    <div id="container">
      <div class="uk-grid">
        <div class="uk-width-1-1">

          <div class="login_form">
            <?php
            // echo $token;
            echo form_open(base_url().'account/set_new_pass', 'class="uk-form " id="myform"');
            echo '<span class="uk-label">Please enter your password and confirm it.</span>';
            echo '</br>';
            echo form_password(array('id' => 'password', 'name' => 'password', 'placeholder' => 'Password'));
            echo '</br>';
            echo form_password(array('id' => 'confirm_password', 'name' => 'confirm_password', 'placeholder' => 'Confirm Password'));
            echo '</br>';
            echo form_hidden('save_new_password', '1');
            echo form_hidden('token', $token);
            echo form_submit(array('id' => 'save_new_password_submit', 'value' => 'Save','class'=>'uk-button'));
            echo '</p>';
            echo form_close();

            echo "<a href='".base_url()."account/forgot_password'>Forgot Password?</a>"

            ?>


          </div>

        </div>
      </div>
    </div>
  </div>
</div>
