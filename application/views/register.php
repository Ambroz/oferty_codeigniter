<div class="login_wrapper">
	<div class="uk-container uk-container-center">
		<div id="container">
			<div class="uk-grid">
				<div class="uk-width-1-1">
					<div class="uk-panel">
						<div class="login">
							<!-- <h1 class="uk-article-title" style="text-align: center">Login</h1> -->
							<div class="login_form">
								<?php
								// $this->load->library('form_validation');
								$options = array();
								foreach($get_country as $key => $value){
									$options[$value->id_country] = $value->country_name;
								}



								echo form_open(base_url().'account/register', 'class="uk-form " id="myform"');

								echo form_input(array('id' => 'email', 'name' => 'email','placeholder' => 'email'));
								echo '</br>';
								echo form_password(array('id' => 'password', 'name' => 'password', 'placeholder' => 'password'));
								echo '</br>';
								echo form_password(array('id' => 'confirm_password', 'name' => 'confirm_password', 'placeholder' => 'confirm password'));
								echo '</br>';
								echo form_input(array('id' => 'firstname', 'name' => 'firstname','placeholder' => 'firstname'));
								echo '</br>';
								echo form_input(array('id' => 'lastname', 'name' => 'lastname','placeholder' => 'lastname'));
								echo '</br>';
								echo form_dropdown('country', $options);
								echo form_hidden('register', '1');
								echo form_hidden('user_group_id', '2');
								echo form_hidden('is_active', '2');
								echo form_submit(array('id' => 'register_submit', 'value' => 'Register','class'=>'uk-button'));
								echo '</p>';
								echo form_close();
								?>

								<p>

								</br>

								<!-- <a href="<?= base_url().'account/forgot' ?>">Ha dimenticato la password?</a> -->
							</p>
						</p>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
