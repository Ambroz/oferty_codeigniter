<?php
// echo "<pre>";
// print_r($get_my_offers);
// echo "</pre>";

?>

<div class="uk-container uk-container-center">
  <div class="uk-width-1-1 uk-row-first">
    <div class="uk-panel uk-panel-box">
      <div id="container">
        <nav class="tm-navbar uk-navbar user_navbar">
          <ul class="uk-navbar-nav uk-nav-default">
            <?php
            if($loginData->user_group_id == 1){?>
              <a class="uk-button uk-button-default buy-plan" href="<?=base_url()."administrator";?>">Back</a>
            <?php  }
            if($loginData->user_group_id == 2){?>
              <li><a href="<?=base_url()?>user/user_profile/edit_profile/<?=$loginData->id_user;?>">Edit Profile</a></li>
              <li><a href="<?=base_url()?>user/user_profile/my_offers">My Offers</a></li>
              <li><a href="<?=base_url()?>user/user_profile/get_plans">My Plans</a></li>
            <?php } ?>
          </ul>
        </nav>
        <?php
        // $date1 = date_create($get_offers->expire_time);
        $date = date('Y-m-d');
        //
        // $today_date = date_create($date);
        //
        // $diff=date_diff($date1,$today_date);
        // echo "Days: ". $diff->format("%a");?>
        <table>
          <tr>
            <th>Offer Title</th>
            <th>Offer Description</th>
            <th>Budget</th>
            <th>Expires In</th>
            <th>Work mode</th>
            <th>Category</th>
            <th>Status</th>
            <th>Operations</th>
          </tr>
          <?php
          foreach($get_my_offers as $my_offers){
            $date1 = date_create($my_offers->expire_time);
            $today_date = date_create($date);

            $diff=date_diff($date1,$today_date);
            ?>

            <tr>
              <td><?=$my_offers->title_offers?></td>
              <td><?=$my_offers->text_offers?></td>
              <td><?=$my_offers->budget?> PLN</td>
              <td><?=$diff->format("%a")?> Days</td>
              <td><?=$my_offers->work_mode_name?></td>
              <td><?=$my_offers->category_name?></td>
              <td><?=$my_offers->status_name?></td>
              <td><a class="uk-button uk-button-default" href="">Details</a></td>
            </tr>
          <?php	}

          ?>
        </table>

      </div>
    </div>
  </div>
</div>
