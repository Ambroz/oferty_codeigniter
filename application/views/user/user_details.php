<div class="uk-container uk-container-center admin-content-wrapper user_list-wrapper">
  <section class="uk-grid">
    <div class="uk-width-1-1">
      <div class="uk-panel user_list-wrapper">
        <nav class="tm-navbar uk-navbar user_navbar">
          <ul class="uk-navbar-nav uk-nav-default">
            <?php
            if($loginData->user_group_id == 1){
              ?>
              <a class="uk-button uk-button-default" href="<?=base_url()?>administrator/main/show_user_list">Back</a>
            <?php }else{
              ?>
              <li><a href="<?=base_url()?>user/user_profile/edit_profile/<?=$loginData->id_user;?>">Edit Profile</a></li>
              <li><a href="<?=base_url()?>user/user_profile/my_offers">My Offers</a></li>
              <li><a href="<?=base_url()?>user/user_profile/get_plans">My Plans</a></li>
              <?php
            }?>
          </ul>
        </nav>
        <?php
        // print_r($get_country);


        $options = array();
        foreach($get_country as $key => $value){
          $options[$value->id_country] = $value->country_name;
        }
        // var_dump($user_details);
        ?>
        User Details


        <?php
        echo form_open(base_url().'user/user_profile/user_update', 'class="uk-form " id="myform"');

        foreach($user_details as $user){
          echo form_hidden('user_id', $user->id_user);
          echo form_input(array('id' => 'login_email', 'name' => 'login_email', 'placeholder' => 'login_email', 'value' => $user->login_email));
          echo '</br>';
          echo form_password(array('id' => 'password', 'name' => 'password', 'placeholder' => 'password'));
          echo '</br>';
          echo form_input(array('id' => 'first_name', 'name' => 'first_name', 'placeholder' => 'first_name', 'value' => $user->first_name));
          echo '</br>';
          echo form_input(array('id' => 'last_name', 'name' => 'last_name', 'placeholder' => 'last_name', 'value' => $user->last_name));
          echo '</br>';
          echo form_input(array('id' => 'address', 'name' => 'address', 'placeholder' => 'address', 'value' => $user->address));
          echo '</br>';
          echo form_dropdown('country_id', $options, $user->id_country);
          echo '</br>';
          if($user->telephon == 0){
            echo form_input(array('id' => 'telephon', 'name' => 'telephon', 'placeholder' => 'telephon', 'value' => ''));
            echo '</br>';
          }else{
            echo form_input(array('id' => 'telephon', 'name' => 'telephon', 'placeholder' => 'telephon', 'value' => $user->telephon));
            echo '</br>';
          }
          if($user->nip == 0){
            echo form_input(array('id' => 'nip', 'name' => 'nip', 'placeholder' => 'nip', 'value' => ''));
            echo '</br>';
          }else{
            echo form_input(array('id' => 'nip', 'name' => 'nip', 'placeholder' => 'nip', 'value' => $user->nip));
            echo '</br>';
          }


          // echo form_input(array('id' => 'user_group_name', 'name' => 'user_group_name', 'placeholder' => 'user_group_name', 'value' => $user->user_group_name));
          // echo '</br>';
          echo form_submit(array('id' => 'update_submit', 'value' => 'Update','class'=>'uk-button'));
          echo '</p>';
          echo form_close();

        }
        ?>

      </div>
    </div>
  </section>
</div>
