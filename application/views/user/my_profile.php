<div class="uk-container uk-container-center">
  <div class="uk-width-1-1 uk-row-first">
    <div class="uk-panel uk-panel-box">
      <div id="container">
        <nav class="tm-navbar uk-navbar user_navbar">
          <ul class="uk-navbar-nav uk-nav-default">
            <?php
            // echo "<pre>";
            // print_r($get_my_profile_info);

            // print_r($show_user);
            // echo '</pre>';

//zalogowany użytkownik
            if (isset($loginData) && $loginData->id_user == $get_my_profile_info[0]->id_user){
              ?>
              <li><a href="<?=base_url()?>user/user_profile/edit_profile/<?=$loginData->id_user;?>">Edit Profile</a></li>
              <li><a href="<?=base_url()?>user/user_profile/my_offers">My Offers</a></li>
              <li><a href="<?=base_url()?>user/user_profile/get_plans">My Plans</a></li>
            </ul>
          </nav>
        </div>
        <ul>
          <?php
          // echo "<pre>";
          // print_r($get_my_profile_info);
          // echo '</pre>';
          foreach ($get_my_profile_info as $key => $profile_info) {?>
            <li><?=$profile_info->first_name." ".$profile_info->last_name?></li>
            <li><?=$profile_info->address?></li>
            <li><?=$profile_info->telephon?></li>
            <li><?=$profile_info->nip?></li>
            <li><?=$profile_info->country_name?></li>
            <?php
          }
          foreach ($count_user_comms as $key => $value) {?>
            <li class="<?=$key?>"><?php echo $key.": ".$value?></li>
          <?php
          }
          ?>
        </ul>
        <?php
        // }
      }else{
        ?>

        <div id="container">
          <div class="nav">
            <?php
            if(isset($loginData)){?>
              <nav class="tm-navbar uk-navbar user_navbar">
                <ul class="uk-navbar-nav uk-nav-default">
                  <!-- <li><a class="uk-button uk-button-default" href="<?//=base_url()."user/user_profile/report_user/".$show_user[0]->id_user."/".$loginData->id_user?>">Report</a></li> -->
                  <li><a class="uk-button uk-button-default modal_report" href="#modal-report" uk-toggle>Report</a></li>

                </ul>
              </nav>
            <?php } ?>
          </div>
          <ul>
            <?php
            echo "<pre>";
            print_r($show_user);
            echo '</pre>';
            foreach ($show_user as $key => $other_user_see_profile_info) {?>
              <li><?=$other_user_see_profile_info->first_name." ".$other_user_see_profile_info->last_name?></li>
              <li><?=$other_user_see_profile_info->address?></li>
              <li><?=$other_user_see_profile_info->telephon?></li>
              <li><?=$other_user_see_profile_info->nip?></li>
              <li><?=$other_user_see_profile_info->country_name?></li>
              <?php
            }
            foreach ($count_user_comms as $key => $value) {?>
              <li class="<?=$key?>"><?php echo $key.": ".$value?></li>
            <?php
            }
            ?>

            </ul>
            <?php
            if(isset($loginData)){
// comment section in commenter profile
              echo form_open(base_url().'user/user_profile/add_user_comm', 'class="uk-form " id="myform"');

              echo form_hidden('user_id', $loginData->id_user);
              echo form_hidden('commented_user', $show_user[0]->id_user);
              echo form_hidden('other_user_profile_first_name', $show_user[0]->first_name);
              echo form_hidden('other_user_profile_last_name', $show_user[0]->last_name);
              echo form_hidden('commenting_user', $loginData->id_user);

              echo form_input(array('id' => 'first_last_name', 'name' => 'first_last_name','placeholder' => 'first_last_name', 'value' => $loginData->first_name." ".$loginData->last_name, 'readonly' => 'true'));
              echo '</br>';
              echo form_textarea(array('id' => 'comment_description', 'class' => 'editor', 'name' => 'comment_description', 'placeholder' => 'Comment'));
              echo '</br>';
              echo form_input(array('id' => 'positive', 'name' => 'type_of_comment', 'type' => 'radio', 'value' => '1', 'checked' => 'TRUE')).'<strong><label for="positive">Positive</label></strong>';
              echo '</br>';
              echo form_input(array('id' => 'neutral', 'name' => 'type_of_comment', 'type' => 'radio', 'value' => '2')).'<strong><label for="neutral">Neutral</label></strong>';
              echo '</br>';
              echo form_input(array('id' => 'negative', 'name' => 'type_of_comment', 'type' => 'radio', 'value' => '3')).'<strong><label for="negative">Negative</label></strong>';
              echo '</br>';

              // echo form_radio("type_of_comment", '1', set_radio("type_of_comment[]", "1")).'<strong><label for="positive">Positive</label></strong>';
              // echo '</br>';
              // echo form_radio("type_of_comment", '2', set_radio("type_of_comment[]", "2")).'<strong><label for="positive">Neutral</label></strong>';
              // echo '</br>';
              // echo form_radio("type_of_comment", '3', set_radio("type_of_comment[]", "3")).'<strong><label for="positive">Negative</label></strong>';
              // echo '</br>';

              // echo form_input(array('id' => 'sallary', 'name' => 'sallary', 'placeholder' => 'sallary', 'type' => 'number'));
              // echo '</br>';
              // echo form_input(array('id' => 'time_duration', 'name' => 'time_duration', 'type' => 'number', 'placeholder' => 'Job Duration'));
              // echo '</br>';
              echo form_hidden('user_comment', '1');
              echo form_submit(array('id' => 'comment_submit', 'value' => 'Send', 'class' => 'uk-button'));
              echo '</p>';
              echo form_close();
            }
          }
          ?>
        </ul>
        User Comments
        <!-- DUPA -->
        <?php
        echo "<pre>";
        print_r($get_user_comments);
        // print_r($count_user_comms);
        // var_dump($count_user_comms);
        echo '</pre>';
        ?>

        <div id="modal-report" uk-modal>
            <div class="uk-modal-dialog uk-modal-body">
                <h2 class="uk-modal-title">Reporting user <?=$show_user[0]->first_name." ".$show_user[0]->last_name?></h2>
                <?php
                echo form_open(base_url().'user/user_profile/report_user/', 'class="uk-form " id="report_form"');

                echo form_hidden('who_is_reported', $show_user[0]->id_user);
                echo form_hidden('who_reports', $loginData->id_user);
                echo form_hidden('other_user_profile_first_name', $show_user[0]->first_name);
                echo form_hidden('other_user_profile_last_name', $show_user[0]->last_name);
                echo form_hidden('seen', '2');
                // echo form_input(array('id' => 'first_last_name', 'name' => 'first_last_name','placeholder' => 'first_last_name', 'value' => $loginData->first_name." ".$loginData->last_name, 'readonly' => 'true'));
                // echo '</br>';
                echo form_textarea(array('id' => 'user_report_text', 'class' => 'editor', 'name' => 'user_report_text', 'placeholder' => 'Report'));
                echo '</br>';

                echo form_hidden('user_report', '1');
                echo form_submit(array('id' => 'comment_submit', 'value' => 'Report', 'class' => 'uk-button'));
                echo '</p>';
                echo form_close();


                ?>
            </div>
        </div>

      </div>
    </div>
  </div>
</div>
