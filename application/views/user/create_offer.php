<?php
// echo "<pre>";
// print_r($offer_category);
// print_r($work_mode);
// echo "</pre>";

?>
<div class="offer_wrapper">
  <div class="uk-container uk-container-center">
    <div id="container">
      <div class="uk-grid">
        <div class="uk-width-1-1">
          <div class="uk-panel">
            <nav class="tm-navbar uk-navbar user_navbar">
              <ul class="uk-navbar-nav uk-nav-default">
                <?php
                if($loginData->user_group_id == 1){?>
                  <a class="uk-button uk-button-default buy-plan" href="<?=base_url()."administrator";?>">Back</a>
                <?php  }
                if($loginData->user_group_id == 2){?>
                  <a class="uk-button uk-button-default buy-plan" href="<?=base_url();?>">Back</a>
                <?php } ?>
              </ul>
            </nav>
            <div class="offer">
              <div class="create_offer_form">
                <?php
                $work_options = array();
                foreach($work_mode as $key => $value){
                  $work_options[$value->id_work_mode] = $value->work_mode_name;
                }

                $category_options = array();
                foreach($offer_category as $key => $value){
                  $category_options[$value->id_category] = $value->category_name;
                }

                echo form_open(base_url().'account/add_offer', 'class="uk-form " id="create_offer_form"');

                // echo form_input(array('id' => 'fname_and_lname', 'name' => 'fname_and_lname','placeholder' => 'First Name and Last Name', 'value' => ''.$loginData->first_name." ".$loginData->last_name.''));
                // echo '</br>';
                echo form_input(array('id' => 'offer_title', 'name' => 'offer_title','placeholder' => 'Offer Title'));
                echo '</br>';
                echo form_dropdown('work_mode', $work_options);
                echo '</br>';
                echo form_dropdown('category', $category_options);
                echo '</br>';
                echo form_input(array('id' => 'budget', 'name' => 'budget', 'type' => 'number', 'placeholder' => 'Max Budget'));
                echo '</br>';
                echo form_input(array('id' => 'expire_time', 'name' => 'expire_time', 'type' => 'date', 'placeholder' => 'Expire Date'));
                echo '</br>';
                echo form_textarea(array('id' => 'job_description', 'class' => 'editor', 'name' => 'job_description', 'placeholder' => 'Job Description'));
                echo '</br>';
                echo form_submit(array('id' => 'offer_submit', 'value' => 'Create Offer', 'class' => 'uk-button'));
                echo '</p>';
                echo form_close();
                ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
