<div class="uk-container uk-container-center admin-content-wrapper user_list-wrapper">
  <!-- <section class="uk-grid"> -->
  <!-- <div class="uk-width-1-1"> -->
  <!-- <div class="uk-panel user_list-wrapper"> -->
  <nav class="tm-navbar uk-navbar user_navbar">
    <ul class="uk-navbar-nav uk-nav-default">
      <?php
      if($loginData->user_group_id == 1){?>
        <a class="uk-button uk-button-default buy-plan" href="<?=base_url()."administrator";?>">Back</a>
      <?php  }
      if($loginData->user_group_id == 2){?>
        <li><a href="<?=base_url()?>user/user_profile/edit_profile/<?=$loginData->id_user;?>">Edit Profile</a></li>
        <li><a href="<?=base_url()?>user/user_profile/my_offers">My Offers</a></li>
        <li><a href="<?=base_url()?>user/user_profile/get_plans">My Plans</a></li>

      <?php } ?>
    </ul>
  </nav>

  <p>
    Plans
  </p>
  <div class="uk-grid">
    <?php
    foreach($get_plans as $plan){
      ?>
      <div class="uk-width-1-4">
        <div class="uk-panel <?=strtolower($plan->name);?>">
          <p class="name"><?=$plan->name;?></p>
          <p class="description"><?=$plan->plan_description;?></p>
          <p class="cost"><?=$plan->cost;?></p>
          <?php
          if($loginData->user_group_id == 1){?>
            <a class="uk-button uk-button-default buy-plan" href="<?=base_url()."administrator/main/edit_plan/".$plan->plan_id;?>">Edit</a>
          <?php  }
          if($loginData->user_group_id == 2){?>
            <a class="uk-button uk-button-default buy-plan" href="<?=base_url()."#"?>">Buy for <?=$plan->cost;?></a>
            <a class="uk-button uk-button-default buy-plan" href="<?=base_url()."#"?>">Buy for Premium Coins</a>
          <?php } ?>
        </div>
      </div>

      <?php
    }
    ?>
  </div>
</div>
