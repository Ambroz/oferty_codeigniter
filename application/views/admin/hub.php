<?php
$this->load->model('admin_model');
?>
<div class="uk-container uk-container-center admin-content-wrapper">
  <section class="uk-grid">
    <div class="uk-width-1-1">
      <p>Amount of Offers in Categories</p>
      <?php
      // echo "<pre>";
      // print_r($offer_categories);
      // echo "<br>";
      // print_r($get_num_offer_from_cat);
      // echo "</pre>";
      $i = 0;
      foreach($offer_categories AS $off_cat){
        $i++;
        ?>
        <div class="proj_name">
          <?php
          $category = $off_cat->category_name;
          echo $category."<br>";
          $get_num = $this->admin_model->get_num_offer_from_cat($category);

          echo $get_num;

          ?>
        </div>
        <?php
        if($i === 5){
          echo '<div class="cleare:both"></div>';
        }
      }
      // echo "<pre>";
      // print_r($report_amount);
      // echo "</pre>";

      ?>
      <div class="warning">
        <div class="war">
          <a href="<?=base_url()?>administrator/main/get_all_reports">
            <img class="war_icon" src="<?=base_url()?>assets/img/icons/warning.png">
            <br/>
            <strong>New Reposrts: <?=$report_amount;?></strong>
          </a>
        </div>
      </div>
    </div>
  </section>
</div>
