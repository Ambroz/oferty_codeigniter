<div class="uk-container uk-container-center">
  <section class="uk-grid">
    <div class="uk-width-1-1">
      <div class="uk-panel">
        <nav class="tm-navbar uk-navbar user_navbar">
          <ul class="uk-navbar-nav uk-nav-default">
            <?php
            if($loginData->user_group_id == 1){?>
              <a class="uk-button uk-button-default buy-plan" href="<?=base_url()."administrator";?>">Back</a>
            <?php  }
            if($loginData->user_group_id == 2){?>
              <a class="uk-button uk-button-default buy-plan" href="<?=base_url();?>">Back</a>
            <?php } ?>
          </ul>
        </nav>
        <?php

        echo form_open(base_url().'administrator/main/add_category', 'class="uk-form " id="create_offer_form"');
        echo form_input(array('id' => 'cat', 'name' => 'cat', 'placeholder' => 'Category Name'));
        echo '</br>';
        echo form_hidden('category_pass', '1');
        echo form_submit(array('id' => 'category_submit', 'name' => 'category_button', 'value' => 'Add Category', 'class' => 'uk-button'));
        echo '</p>';
        echo form_close();

        ?>

        <table>
          <tr>
            <th>Offer Categories</th>
          </tr>
          <?php
          foreach($get_category AS $key => $value){?>
            <tr>
              <td><?=$value->category_name;?></td>
            </tr>
            <?php
          }
          ?>
        </table>
      </div>
    </div>
  </section>
</div>
