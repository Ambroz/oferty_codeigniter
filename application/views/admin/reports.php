<div class="uk-container uk-container-center admin-content-wrapper">
  <section class="uk-grid">
    <div class="uk-width-1-1">
        <a class="uk-button uk-button-default" href="<?=base_url()."administrator";?>">Back</a>
      <?php
echo "<pre>";
print_r($all_reports);
echo "</pre>";

      ?>

      <table class="report_table">
        <tr>
          <th>Reported Login</th>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Country</th>
          <th>Strikes</th>
          <th>Seen?</th>
          <th>Operations</th>
        </tr>
        <?php
        foreach ($all_reports as $key => $value) {
          $name = $value->first_name."_".$value->last_name;
          $who_reported = $value->who_reported_id;
          $who_is_reported = $value->who_is_reported_id;
          $report_id = $value->report_id;
          ?>

          <tr>
            <td><?=$value->login_email?></td>
            <td><?=$value->first_name?></td>
            <td><?=$value->last_name?></td>
            <td><?=$value->short_code?></td>
            <td><?=$value->amount_of_strikes?></td>
            <?php if($value->seen_id == 1){ ?>
            <td style="background-color: green; color: white; font-weight: boldl"><?=$value->seen_name?></td>
          <?php
        }else{?>
            <td style="background-color: red; color: white; font-weight: boldl"><?=$value->seen_name?></td>
            <?php
        }?>
            <td><a class="uk-button uk-button-default" href="<?=base_url()."administrator/main/report_details/".$name."/".$who_is_reported."/".$who_reported."/".$report_id."/".$value->amount_of_strikes;?>">View</a></td>
          </tr>
        <?php
      }

        ?>
      </table>
    </div>
  </section>
</div>
