<!-- <div class="menu-wrapper"> -->
<!-- <div class="uk-container uk-container-center admin-menu-wrapper"> -->
<!-- <section class="uk-grid"> -->
<!-- <div class="uk-width-1-1"> -->
<!-- <ul class="uk-navbar uk-navbar-nav left uk-flex uk-flex-middle"> -->
<nav class="tm-navbar uk-navbar admin_navbar">
  <ul class="uk-navbar-nav uk-nav-default">
    <?php
    if (isset($loginData)){
      if($loginData->user_group_id == 1){
        $host = "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
        if(strpos($host,'administrator') !== false){
        ?>
        <li><a href="<?=base_url()?>">Main Page</a></li>
      <?php }else{ ?>
        <li><a href="<?=base_url()?>administrator">Admin Panel</a></li>
      <?php } ?>
        <li><a href="<?=base_url()?>account/create_offer">Create Offer</a></li>
        <li><a href="<?=base_url()?>administrator/main/add_category">Add Category</a></li>
        <li><a href="<?=base_url()?>user/user_profile">My Profile</a></li>
        <li><a href="<?=base_url()?>administrator/main/show_user_list">User List</a></li>
        <li><a href="<?=base_url()?>user/user_profile/get_plans">Plans</a></li>
        <li><a href="<?=base_url()?>account/logout">Logout</a></li>
        <?php
      }
    }
    ?>
  </ul>
</nav>
<!-- </div> -->
<!-- </section> -->
<!-- </div> -->
<!-- </div> -->
