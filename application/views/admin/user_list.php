<?php
// echo "<pre>";
//
// foreach($user_list as $key => $value){
//   print_r($value);
// }
?>

<div class="uk-container uk-container-center admin-content-wrapper user_list-wrapper">
  <section class="uk-grid">
  <div class="uk-width-1-1">
    <a class="uk-button uk-button-default" href="<?=base_url()?>administrator">Back</a>
    <table>
      <tr>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Login</th>
        <th>Adress</th>
        <th>Country</th>
        <th>Telephone</th>
        <th>NIP</th>
        <th>Group</th>
        <th>Active</th>
        <th>Operations</th>
      </tr>

      <?php
      foreach($user_list as $key => $value){?>
        <tr>
          <td><?=$value->first_name;?></td>
          <td><?=$value->last_name;?></td>
          <td><?=$value->login_email;?></td>
          <td><?=$value->address;?></td>
          <td><?=$value->country_name;?></td>
          <td><?=$value->telephon;?></td>
          <td><?=$value->nip;?></td>
          <td><?=$value->user_group_name;?></td>
          <td><?=$value->active_name;?></td>
          <td><a class ="uk-button uk-button-default" href="<?=base_url()."user/user_profile/edit_profile/".$value->id_user?>">Details</a></td>
        </tr>
      <?php }?>

    </table>
  </div>
</section>
</div>
