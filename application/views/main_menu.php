<div class="menu-wrapper">
  <div class="uk-container uk-container-center menu-wrapper">
    <div class="uk-grid">
      <div class="uk-width-1-1">
        <ul class="uk-navbar uk-navbar-nav left uk-flex uk-flex-middle">
          <?php
          if (isset($loginData)){
            ?>
            <li><a href="<?=base_url()?>">About Us</a></li>
            <?php
            if($loginData->user_group_id == 2){?>
            <li><a href="<?=base_url()?>account/create_offer">Create Offer</a></li>
          <?php } ?>
            <li><a href="<?=base_url()?>">Contact</a></li>
          <?php }else{ ?>
            <li><a href="<?=base_url()?>">About Us</a></li>
            <li><a href="<?=base_url()?>">Contact</a></li>
          <?php }?>
        </ul>
      </div>
    </div>
  </div>
</div>
