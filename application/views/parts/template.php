<?=$header?>


<? if(!empty($logo)):?>
<?=$logo ; ?>

<?php
//show menu if any
if(isset($menu) && !empty($menu)) echo $menu;
if(isset($user_menu) && !empty($user_menu)) echo $user_menu;
if(isset($admin_menu) && !empty($admin_menu)) echo $admin_menu; ?>
<?php
//show message if any
if (isset($message) && !empty($message)){
	echo "<div class='uk-container alerts'>";
	echo "<div class='uk-alert'>".$message."</div>";
	echo "</div>";
}
?>
<? if(!empty($content)): ?>

<?=$content ;?>
<? endif; ?>

<?=$footer?>
