<div class="footer-menu-wrapper">
	<div class="uk-container uk-container-center">
	<footer>
	<div class="uk-grid">
				<div class="uk-width-1-1 uk-flex uk-flex-middle">
					<div class="uk-panel">
						<ul class="uk-navbar uk-navbar-nav copy">
							<li>Copyright 2019-<?=date("Y")?> by Kraina Ofert, Made by <strong>Eternal Guardians</strong></li>
						</ul>
					</div>
				</div>
</div>

	</div>
	</div>
	</div>
</footer>
</div>
<script type="text/javascript">
		base_url = '<?=base_url()?>';
</script>
</body>
</html>
