<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
		$this->load->library('email');
		$this->load->library('form_validation');
	}

	public function index()
	{
		if (isset($this->loginData->user_group_id)) {
			redirect('main');
		} else {
			$this->_viewData['logo'] = $this->load->view('logo', $this->_viewData, true);
			$this->_viewData['user_menu'] = $this->load->view('user/menu', $this->_viewData, true);
			$this->_viewData['content'] = $this->load->view('login', $this->_viewData, true);
			$this->load->view('parts/template', $this->_viewData);
		}
	}

	public function login()
	{
		if (null !== $this->input->post('login')) {
			//tu powinna być WALIDACJA
			$this->form_validation->set_rules('email', 'Email', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required');

			if ($this->form_validation->run() == FALSE) {

				$this->session->set_flashdata('message', "Check your E-mail and Password");

				redirect('account');
			} else {
				$email = $this->input->post('email');
				$password = $this->input->post('password');

				$log = $this->user_model->login($email, $password);
				if (!is_object($log)) {
					if ($log == 'inactive')
					$this->session->set_flashdata('message', "Konto nie jest aktywne. Odwiedź swój e-mail, aby aktywować konto.");
					redirect('account');
				} else {
					$this->session->set_userdata('user', $log);
					if ($log->user_group_id == 1) {
						redirect('administrator');
					} else {
						redirect('');
					}
				}
			}
		} else {
			redirect('account');
		}
	}

	public function register()
	{
		if (null !== $this->input->post('register')) {
			$fields = [ 'email','password','confirm_password','firstname','lastname','user_group_id','is_active','country'];
			// przypisanie postów do zmiennych i sprawdzenie czy są jakieś puste wartości
			$empty_post_variables = [];
			foreach ($fields as $f ) {
				$$f = $this->input->post($f);
				if( !isset($$f) ) {
					$empty_post_variables[] = $f;
				}
			}

			if(count($empty_post_variables)) {
				// wypisanie jakie pola są puste
				redirect('account/register');
			}

			if($password !== $confirm_password) {
				//hasła nie są takie same
				$this->session->set_flashdata('message', "The Passwords don't match");
				redirect('account/register');
			}

			if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
				// info o złym mailu
				$this->session->set_flashdata('message', "The Email isn't Valid");
				redirect('account/register');
			}
		}else{

			$this->_viewData['get_country'] = $this->user_model->get_country();
			$this->_viewData['logo'] = $this->load->view('logo', $this->_viewData, true);
			$this->_viewData['user_menu'] = $this->load->view('user/menu', $this->_viewData, true);
			$this->_viewData['content'] = $this->load->view('register', $this->_viewData, true);
			$this->load->view('parts/template', $this->_viewData);

			$reg = $this->user_model->register($email, $password, $firstname, $lastname, $user_group_id, $is_active, $country);
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('');
	}

	public function create_offer()
	{
		$this->_viewData['logo'] = $this->load->view('logo', $this->_viewData, true);
		if (isset($this->loginData)) {
			if ($this->loginData->user_group_id == 1) {
				$this->_viewData['admin_menu'] = $this->load->view('admin/menu', $this->_viewData, true);
			}
			if ($this->loginData->user_group_id == 2) {
				$this->_viewData['user_menu'] = $this->load->view('user/menu', $this->_viewData, true);
			}
		}
		$this->_viewData['offer_category'] = $this->user_model->get_offer_category();
		$this->_viewData['work_mode'] = $this->user_model->get_work_mode();
		$this->_viewData['content'] = $this->load->view('user/create_offer', $this->_viewData, true);
		$this->load->view('parts/template', $this->_viewData);
	}

	public function add_offer(){
		//tu powinna być WALIDACJA

		$offer_title = $this->input->post('offer_title');
		$work_mode = $this->input->post('work_mode');
		$category = $this->input->post('category');
		$budget = $this->input->post('budget');
		$expire_time = $this->input->post('expire_time');
		$job_description = $this->input->post('job_description');

		$user_add_offer = $this->loginData->id_user;
		// print_r($this->loginData->id_user);
		$create = $this->user_model->create_offer($user_add_offer, $offer_title, $work_mode, $category, $budget, $expire_time, $job_description);
	}


	public function forgot_password(){
		$this->_viewData['logo'] = $this->load->view('logo', $this->_viewData, true);
		$this->_viewData['content'] = $this->load->view('forgot_password', $this->_viewData, true);
		$this->load->view('parts/template', $this->_viewData);
	}

	public function remind_me(){
		if($this->input->post("forgot_password") !== null){
			$email = $this->input->post("email");
			$token = $this->_viewData['gen_token'] = $this->user_model->gen_token($email);

			//send Email
			$this->session->set_flashdata('message', 'Soon there sould arrive an e-mail with the reset link');
			// redirect('account/forgot_password');
			redirect('account/reset/'.$token.'');
		}
	}

	public function reset($token){
		$this->_viewData['logo'] = $this->load->view('logo', $this->_viewData, true);
		$this->_viewData['token'] = $token;
		$this->_viewData['content'] = $this->load->view('reset', $this->_viewData, true);
		$this->load->view('parts/template', $this->_viewData);
	}

	public function set_new_pass(){

		if($this->input->post('save_new_password') !== null){
			$token = $this->input->post('token');
			if(isset($token)){
				$password = $this->input->post('password');
				$confirm_password = $this->input->post('confirm_password');

				if($password !== $confirm_password) {
					//hasła nie są takie same
					$this->session->set_flashdata('message', "The Passwords don't match");
					redirect('account/reset/'.$token.'');
				}else{
					$this->_viewData['update_pass'] = $this->user_model->update_pass($password, $token);

					$this->session->set_flashdata('message', "The password has been changed");

					redirect("");
				}
			}
		}
	}
}
