<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends MY_Controller {
  public function __construct()
  {
    parent::__construct();

    $this->load->model('admin_model');
    $this->load->model('user_model');
  }


  public function index(){
    if($this->loginData->user_group_id == 1){

      $this->_viewData['offer_categories'] = $this->admin_model->get_offer_categories();
      $this->_viewData['report_amount'] = $this->admin_model->get_report_amount();
      $this->_viewData['logo'] = $this->load->view('logo', $this->_viewData, true);
      $this->_viewData['menu'] = $this->load->view('admin/menu', $this->_viewData, true);
      $this->_viewData['content'] = $this->load->view('admin/hub', $this->_viewData, true);
      $this->load->view('parts/template', $this->_viewData);
    }else{
      // $this->session->set_flashdata('message', "Wstęp Wzbroniony");
      redirect('account/login');
    }
  }


  public function show_user_list(){
    if($this->loginData->user_group_id == 1){

      $this->_viewData["user_list"] = $user_list = $this->admin_model->get_all_users();
      // var_dump($country);


      $this->_viewData['logo'] = $this->load->view('logo', $this->_viewData, true);
      $this->_viewData['menu'] = $this->load->view('admin/menu', $this->_viewData, true);
      $this->_viewData['content'] = $this->load->view('admin/user_list', $this->_viewData, true);
      $this->load->view('parts/template', $this->_viewData);
    }else{
      $this->session->set_flashdata('message', "Wstęp Wzbroniony");
      redirect('');
    }
  }

  public function show_user_details($id){
    if($this->loginData->user_group_id == 1){

      $this->_viewData["user_details"] = $this->admin_model->get_user_details($id);

      $this->_viewData['logo'] = $this->load->view('logo', $this->_viewData, true);
      $this->_viewData['menu'] = $this->load->view('admin/menu', $this->_viewData, true);
      $this->_viewData['content'] = $this->load->view('admin/user_details', $this->_viewData, true);
      $this->load->view('parts/template', $this->_viewData);

    }
  }

  public function edit_plan($id){
    if($this->loginData->user_group_id == 1){
      $this->_viewData["edit_plan"] = $this->admin_model->get_edit_plan($id);

      $this->_viewData['logo'] = $this->load->view('logo', $this->_viewData, true);
      $this->_viewData['menu'] = $this->load->view('admin/menu', $this->_viewData, true);
      $this->_viewData['content'] = $this->load->view('admin/edit_plan', $this->_viewData, true);
      $this->load->view('parts/template', $this->_viewData);
    }
  }

  public function add_category(){
    if($this->loginData->user_group_id == 1){
      if(null!==$this->input->post('category_pass')){
        $this->form_validation->set_rules('cat', 'Category', 'required',
        array(
          'required'      => $this->session->set_flashdata('message','You have not provided the category'),
        )
      );
      if ($this->form_validation->run() == FALSE)
      {

        // $this->session->set_flashdata('message', "Konto o podanym adresie email już istnieje");

        redirect('administrator/main/add_category');
      }
      else
      {
        $category = $this->input->post('cat');
        $this->_viewData["add_category"] = $this->admin_model->add_category_to_db($category);
      }
    }
    $this->_viewData["get_category"] = $this->user_model->get_offer_category();

    $this->_viewData['logo'] = $this->load->view('logo', $this->_viewData, true);
    $this->_viewData['menu'] = $this->load->view('admin/menu', $this->_viewData, true);
    $this->_viewData['content'] = $this->load->view('admin/add_category', $this->_viewData, true);
    $this->load->view('parts/template', $this->_viewData);
  }
}

public function get_all_reports(){
  if($this->loginData->user_group_id == 1){
    $this->_viewData['all_reports'] = $this->admin_model->get_all_reports();

    $this->_viewData['logo'] = $this->load->view('logo', $this->_viewData, true);
    $this->_viewData['menu'] = $this->load->view('admin/menu', $this->_viewData, true);
    $this->_viewData['content'] = $this->load->view('admin/reports', $this->_viewData, true);
    $this->load->view('parts/template', $this->_viewData);
  }
}

public function report_details($name, $who_is_reported, $who_reported, $report_id, $amount_of_strikes){
  if($this->loginData->user_group_id == 1){
    $this->_viewData['get_spec_report_info'] = $this->admin_model->get_spec_report_info($report_id);

    $this->_viewData['reported_user_name'] = $name;
    $this->_viewData['who_is_reported'] = $who_is_reported;
    $this->_viewData['report_id'] = $report_id;
    $this->_viewData['amount_of_strikes'] = $amount_of_strikes;

    $this->_viewData['logo'] = $this->load->view('logo', $this->_viewData, true);
    $this->_viewData['menu'] = $this->load->view('admin/menu', $this->_viewData, true);
    $this->_viewData['content'] = $this->load->view('admin/detailed_report', $this->_viewData, true);
    $this->load->view('parts/template', $this->_viewData);
  }
}

public function reports(){
  if($this->loginData->user_group_id == 1){
    $who_is_reported = $this->input->post('who_is_reported');
    $report_id = $this->input->post('report_id');
    $admin_mod_text = $this->input->post('admin_mod_text');
    $who_reported = $this->input->post('who_reported');
    $seen = $this->input->post('seen');

    $check_list = $this->input->post('check_list[]');
    if(null!==$this->input->post('reports')){
      // if(isset($check_list[0]) && isset($check_list[1])){
      //   // echo 'ustawiono usunuęcie raportu i usunuęcie strike';
      //   $this->admin_model->delete_rep_and_strike($report_id, $who_is_reported);
      //
      // }else if(isset($check_list[1])){
      //   $this->admin_model->delete_rep($report_id, $who_is_reported);
      //
      //
      // }else
      if(isset($check_list[0])){
        $this->form_validation->set_rules('admin_mod_text', 'admin_mod_text', 'required',
        array(
          'required'      => $this->session->set_flashdata('message','Please provided the response'),
        )
      );
      if ($this->form_validation->run() == FALSE){

        // $this->session->set_flashdata('message', "Konto o podanym adresie email już istnieje");

        redirect('administrator/main/report_details/Paw_A/'.$who_is_reported.'/'.$who_reported.'/'.$report_id.'');

      }else{
        $this->session->set_flashdata('message','wykonana część 1');
        $this->admin_model->delete_strike($report_id, $who_is_reported, $admin_mod_text, $seen);
      }
    }else{
      $this->form_validation->set_rules('admin_mod_text', 'admin_mod_text', 'required',
      array(
        'required'      => $this->session->set_flashdata('message','Please provided the response'),
      )
    );
    if ($this->form_validation->run() == FALSE){
      redirect('administrator/main/report_details/Paw_A/'.$who_is_reported.'/'.$who_reported.'/'.$report_id.'');
    }else{
      $this->session->set_flashdata('message','wykonana część 2');
      $this->admin_model->add_rep_text($report_id, $who_is_reported, $admin_mod_text, $seen);

    }
  }
  // if ($this->form_validation->run() == FALSE)
  // {
  //
  //   // $this->session->set_flashdata('message', "Konto o podanym adresie email już istnieje");
  //
  //   redirect('administrator/main/report_details/Paw_A/'.$who_is_reported.'/'.$who_reported.'/'.$report_id.'');
  // }
  // else


}
}
}
}
