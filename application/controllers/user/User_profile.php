<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_profile extends MY_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model('user_model');
    $this->load->library('email');
  }

  public function index(){
    var_dump($this->loginData->id_user);
    if (!isset($this->loginData->user_group_id))
    {
      redirect('main');
    }
    else
    {
      $this->_viewData['logo'] = $this->load->view('logo', $this->_viewData, true);
      if($this->loginData->user_group_id == 2){
        $this->_viewData['user_menu'] = $this->load->view('user/menu', $this->_viewData, true);
      }else{
        $this->_viewData['user_menu'] = $this->load->view('admin/menu', $this->_viewData, true);
      }
      $this->_viewData['get_my_profile_info'] = $this->user_model->get_my_profile_info($this->loginData->id_user);
      $this->_viewData['get_user_comments'] = $this->user_model->get_user_comments($this->loginData->id_user);
      $this->_viewData['count_user_comms'] = $this->user_model->count_coms($this->loginData->id_user);
      $this->_viewData['content'] = $this->load->view('user/my_profile', $this->_viewData, true);
      $this->load->view('parts/template', $this->_viewData);
    }
  }

  public function edit_profile($id){
    $this->_viewData['logo'] = $this->load->view('logo', $this->_viewData, true);
    $this->_viewData["user_details"] = $this->user_model->get_user_details($id);
    $this->_viewData['get_country'] = $this->user_model->get_country();

    if($this->loginData->user_group_id == 1){
      $this->_viewData['admin_menu'] = $this->load->view('admin/menu', $this->_viewData, true);
    }

    if($this->loginData->user_group_id == 2){
      $this->_viewData['user_menu'] = $this->load->view('user/menu', $this->_viewData, true);
    }

    $this->_viewData['content'] = $this->load->view('user/user_details', $this->_viewData, true);
    $this->load->view('parts/template', $this->_viewData);
  }

  public function my_offers(){
    $id = $this->loginData->id_user;
    $this->_viewData['logo'] = $this->load->view('logo', $this->_viewData, true);
    if($this->loginData->user_group_id == 1){
      $this->_viewData['admin_menu'] = $this->load->view('admin/menu', $this->_viewData, true);
    }

    if($this->loginData->user_group_id == 2){
      $this->_viewData['user_menu'] = $this->load->view('user/menu', $this->_viewData, true);
    }
    $this->_viewData['get_my_offers'] = $this->user_model->get_my_offers($id);
    $this->_viewData['content'] = $this->load->view('user/my_offers', $this->_viewData, true);
    $this->load->view('parts/template', $this->_viewData);
  }

  public function user_update(){
    $edit_user = $this->input->post('user_id');
    $id = $this->loginData->id_user;
    $login_email = $this->input->post('login_email');
    $pass = password_hash($this->input->post('password'), PASSWORD_BCRYPT);
    $first_name = $this->input->post('first_name');
    $last_name = $this->input->post('last_name');
    $address = $this->input->post('address');
    $country_id = $this->input->post('country_id');

    if($this->input->post('telephon') === 0){
      $telephon = 'NULL';
    }else{
      $telephon =	$this->input->post('telephon');
    }

    if($this->input->post('nip') === 0){
      $nip = 'NULL';
    }else{
      $nip =	$this->input->post('nip');
    }

    $data = array(
      'login_email' => $login_email,
      'password' => $pass,
      'first_name' => $first_name,
      'last_name' => $last_name,
      'address' => $address,
      'country_id' => $country_id,
      'telephon' => $telephon,
      'nip' => $nip
    );

    $this->user_model->user_update($data, $id, $edit_user);
  }

  public function get_plans(){
    $this->_viewData["get_plans"] = $this->user_model->get_plans();
    $this->_viewData['logo'] = $this->load->view('logo', $this->_viewData, true);
    if($this->loginData->user_group_id == 1){
      $this->_viewData['admin_menu'] = $this->load->view('admin/menu', $this->_viewData, true);
    }

    if($this->loginData->user_group_id == 2){
      $this->_viewData['user_menu'] = $this->load->view('user/menu', $this->_viewData, true);
    }
    $this->_viewData['content'] = $this->load->view('user/plans', $this->_viewData, true);
    $this->load->view('parts/template', $this->_viewData);
  }

  public function user_show_profile($name, $id){

    $this->_viewData['logo'] = $this->load->view('logo', $this->_viewData, true);
    if($this->loginData->user_group_id == 1){
      $this->_viewData['admin_menu'] = $this->load->view('admin/menu', $this->_viewData, true);
    }

    if($this->loginData->user_group_id == 2){
      $this->_viewData['user_menu'] = $this->load->view('user/menu', $this->_viewData, true);
    }
    $this->_viewData['show_user'] = $this->user_model->get_my_profile_info($id);
    $this->_viewData['get_user_comments'] = $this->user_model->get_user_comments($id);

    $this->_viewData['count_user_comms'] = $this->user_model->count_coms($id);
    // $this->_viewData['count_user_neutral_comms'] = $this->user_model->count_coms_neut($id);
    // $this->_viewData['count_user_positive_comms'] = $this->user_model->count_coms_neg($id);

    $this->_viewData['content'] = $this->load->view('user/my_profile', $this->_viewData, true);
    $this->load->view('parts/template', $this->_viewData);
  }

  public function add_user_comm(){
    $id_commented = $this->input->post('commented_user');
    $id_commenting = $this->input->post('commenting_user');
    $first_name = $this->input->post('other_user_profile_first_name');
    $last_name = $this->input->post('other_user_profile_last_name');
    $full_name = $first_name."_".$last_name;
    $date = date('Y-m-d');


    if(null!==$this->input->post('user_comment')){
      $this->form_validation->set_rules('comment_description', 'Comment Description', 'required',
      array(
        'required'      => $this->session->set_flashdata('message','You have not provided the Comment.'),
      )
    );
    // 	$this->form_validation->set_rules('type_of_comment', 'Type of Comment', 'required',
    // 	array(
    // 		'required'      => $this->session->set_flashdata('message','Please select a Comment Type'),
    // 	)
    // );

    if ($this->form_validation->run() == FALSE){

      // $this->session->set_flashdata('message', "Please Fill all the fields");

      redirect('user/user_profile/user_show_profile/'.$full_name.'/'.$id_commented);
    }else{
      // echo $id_commented; //osoba, którą komentujemy
      // echo "<br>";
      // echo $id_commenting; //komentujący
      // echo "<br>";
      $comm_desc = $this->input->post('comment_description'); //komentarz
      // echo "<br>";
      $comm_type = $this->input->post('type_of_comment'); //typ komentarza

      $this->user_model->add_user_comm_to_db($id_commenting, $comm_desc, $comm_type, $id_commented, $full_name, $date);
    }
  }
}

public function report_user(){
  $date_of_report = date('Y-m-d');
  $first_name = $this->input->post('other_user_profile_first_name');
  $last_name = $this->input->post('other_user_profile_last_name');
  $full_name = $first_name."_".$last_name;
  $who_is_reported = $this->input->post('who_is_reported');

  if(null !== $this->input->post('user_report')){
    $this->form_validation->set_rules('user_report_text', 'User Report', 'required',
    array(
      'required'      => $this->session->set_flashdata('message','Please fill the Report area'),
    )
  );
  if ($this->form_validation->run() == FALSE){

    // $this->session->set_flashdata('message', "Please Fill all the fields");

    redirect('user/user_profile/user_show_profile/'.$full_name.'/'.$who_is_reported);
  }else{

    $who_reported = $this->input->post('who_reports');
    $report = $this->input->post('user_report_text');
    $seen = $this->input->post('seen');

    $this->user_model->add_user_report($who_is_reported, $who_reported, $report, $date_of_report, $full_name, $seen);

  }
}
}
}
