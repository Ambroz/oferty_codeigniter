<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Main extends MY_Controller
	{
		public function __construct()
		{
			parent::__construct();
			$this->load->model('user_model');
			$this->load->library('email');
		}
		
		/**
		 * Index Page for this controller.
		 *
		 * Maps to the following URL
		 *        http://example.com/index.php/welcome
		 *    - or -
		 *        http://example.com/index.php/welcome/index
		 *    - or -
		 * Since this controller is set as the default controller in
		 * config/routes.php, it's displayed at http://example.com/
		 *
		 * So any other public methods not prefixed with an underscore will
		 * map to /index.php/welcome/<method_name>
		 * @see https://codeigniter.com/user_guide/general/urls.html
		 */
		public function index()
		{
			var_dump($this->session->get_userdata());
			$this->_viewData['logo'] = $this->load->view('logo', $this->_viewData, true);
			if (isset($this->loginData)) {
				if ($this->loginData->user_group_id == 1) {
					$this->_viewData['admin_menu'] = $this->load->view('admin/menu', $this->_viewData, true);
				}
				if ($this->loginData->user_group_id == 2) {
					$this->_viewData['user_menu'] = $this->load->view('user/menu', $this->_viewData, true);
				}
			}
			$this->_viewData['get_offers'] = $this->user_model->get_offers();
			$this->_viewData['content'] = $this->load->view('main_page', $this->_viewData, true);
			$this->load->view('parts/template', $this->_viewData);
		}
		
		public function show_offer($offer_name, $offer_id)
		{
			$this->_viewData['logo'] = $this->load->view('logo', $this->_viewData, true);
			if (isset($this->loginData)) {
				if ($this->loginData->user_group_id == 1) {
					$this->_viewData['admin_menu'] = $this->load->view('admin/menu', $this->_viewData, true);
				}
				if ($this->loginData->user_group_id == 2) {
					$this->_viewData['user_menu'] = $this->load->view('user/menu', $this->_viewData, true);
				}
			}
			$this->_viewData['sel_offer'] = $this->user_model->get_info_of_selected_offer($offer_id);
			$this->_viewData['sel_comm'] = $this->user_model->sel_comments($offer_id);
			$this->_viewData['content'] = $this->load->view('offer', $this->_viewData, true);
			$this->load->view('parts/template', $this->_viewData);
		}
		
		public function add_comm_to_offer()
		{
			$sel_offer_id = $this->input->post('sel_offer_id');
			$title_offer = $this->input->post('title_offer');
			
			if (null !== $this->input->post('comment')) {
				$this->form_validation->set_rules('sallary', 'Sallary', 'required');
				$this->form_validation->set_rules('time_duration', 'Time Duration', 'required');
				$this->form_validation->set_rules('comment_description', 'Comment Description', 'required');
				
				if ($this->form_validation->run() == FALSE) {
					
					$this->session->set_flashdata('message', "All the fields must be filled");
					
					redirect('main/show_offer/' . $title_offer . '/' . $sel_offer_id);
				} else {
					
					$user_id = $this->input->post('user_id');
					$sallary = $this->input->post('sallary');
					$time_duration = $this->input->post('time_duration');
					$comment_description = $this->input->post('comment_description');
					
					$comm_data = array(
						'commenter_user_id' => $user_id,
						'comment' => $comment_description,
						'sallary' => $sallary,
						'realization_time' => $time_duration
					);
					
					$this->user_model->add_comm_to_db($comm_data, $sel_offer_id, $title_offer);
				}
			}
		}
		
		public function contact()
		{
			$this->_viewData['logo'] = $this->load->view('logo', $this->_viewData, true);
			if (isset($this->loginData)) {
				if ($this->loginData->user_group_id == 1) {
					$this->_viewData['admin_menu'] = $this->load->view('admin/menu', $this->_viewData, true);
				}
				if ($this->loginData->user_group_id == 2) {
					$this->_viewData['user_menu'] = $this->load->view('user/menu', $this->_viewData, true);
				}
			}
			$this->_viewData['get_offers'] = $this->user_model->get_offers();
			$this->_viewData['content'] = $this->load->view('contact', $this->_viewData, true);
			$this->load->view('parts/template', $this->_viewData);
		}
		
		
	}
